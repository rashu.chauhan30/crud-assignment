import helmet from 'helmet';
import { routes } from "./routes.js";

export const registerMiddlewares = (app) => {
      app.use(helmet());

      app.use((req, res, next) => {
            console.log("THE SERVER WAS ACCESSED");
            next();
      })

      for(let route of routes) {
            app.use(route.path, route.handler);
      }

      app.use((err, req, res, next)=> {

      });
}
