import express from "express";
import { registerMiddlewares } from './register-middlewares.js'

export const startServer = () => {
      const app = express();

      registerMiddlewares(app);

      app.listen(
            process.env.PORT,
            () => console.log(`SERVER STARTED ON PORT ${ process.env.PORT}`)
      )
}